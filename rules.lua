--The actual rules.
interact.rules = [[
============================================
YOU MUST READ AND  AGREE TO THESE RULES
============================================


This server is a pro-LGBTQ+ server. This means that there is a ZERO tolerance policy for any of the following:


1.1. Bigotry agaisnt the LGBTQ+ community. This includes using slurs, spreading bigoted beliefs, or celebrating or advocating for hate crimes. It can also be more subtle and include discussions about if gay or trans people should be allowed to do this or that. We understand there are cultural attitudes towards LGBTQ+ people, please have an open mind.

1.2. Showing support for politicians or laws that harm the LGBTQ+ community. Ex: Trump support. This is up to interpretation. If the politician is causing harm to the LGBTQ+ community as a major part of their platform this is not allowed.

1.3. Using religion as an excuse to spread hateful ideas. It doesn't matter if you consider it part of your religion, if the belief is harmful to the LGBTQ+ community we will not tolerate it here. We're not against religion here, only bigotry.

1.4. Asking players questions about their gender or sexuality, or invalidating their gender or sexuality. This server is not a discussion, please do not ask anyone personal questions unless they gave you permission first. Please don't ask if someone is really a boy/girl/whatever, or if they are really a gay/lesbian/bisexual person. Assume that they are what they say they are.


Here are some generic server rules to keep in mind:


2.1. This is a family friendly server. Please don't talk about adult topics, like sexual topics, drugs, etc. Gender and sexuality are not inherently sexual topics. Children can be lesbian, gay, bisexual, and/or transgender, queer, etc. We want to be a safe space for LGBTQ+ kids too.

2.2. Be nice to everyone. Do not insult or verbally abuse any of our players. We won't tolerate verbally abusing any of our staff members.

2.3. No griefing. You will be instantly banned if you grief anything we built or the wilderness.

2.4. Don't ask for ranks or privs often. Asking for track_builder or mesecons privs is fine if you actually need them. If you want to not be an admin or mod, ask for it as much as possible.

2.5. Claim your areas! Use /area_pos1 and /area_pos2 then /protect <name> to claim your own area. You can also use markers. We have an area example room at spawn.

2.6. PVP is optional and is off by default. Use /toggle_pvp to enable/disable for yourself.

2.7. When building tracks, try to make them realistic. The length, width, and height of 1 node is 1 meter. The length, width, and height of 1 mapblock is 16 meters. Only raise/lower the track elevation by 1 meter at least every 48 meters (3 mapblocks). Only make a turn in the tracks at least every 352 meters (22 mapblocks). This rule will only be enforced for intercity rails. You can build tracks however you want as long as the track doesn't go too far or connect to other player's bases/cities. This is up to interpretation.

2.8. When using a digtron, for every 20 digger heads you must add 1 second to the delay. The maximum amount of heads is 128. Dual digger heads count as 1 head.


These rules are subject to change. Holly has final word in all matters.

This server is meant as a safe place for members of the LGBTQ+ community to play a fun game and talk with others like themselves. Players on this server should either be a member of the LGBTQ+ community or an ally of the LGBTQ+ community. Anyone here to cause disruption is subject to immediate ban without explanation.

In most cases the worst that will happen to you is getting kicked. If it's something minor, like saying the wrong thing or not using the right word for something, we will let you know. If you are told to stop doing something but repeatedly keep doing it, you will be banned. With some things, like using slurs or griefing, you will be banned with no warning.

We highly encourage players to let other people in the LGBTQ+ community know about this server and how to get here to play and chat. There is a lot of hate in the world and this server is meant to be a safe place for all members of the LGBTQ+ community.

Remember, you are valid, you are loved, and you are not broken.
-Holly and livphi
]]

--The questions on the rules, if the quiz is used.
--The checkboxes for the first 4 questions are in config.lua
interact.s4_question1 = "Is PVP is allowed/optional?"
interact.s4_question2 = "Is sexual talk allowed?"
interact.s4_question3 = "Should you be nice to all players?"
interact.s4_question4 = "Should you ask for all the privs you can?"
interact.s4_multi_question = "Which of these is a rule?"

--The answers to the multiple choice questions. Only one of these should be true.
interact.s4_multi1 = "No griefing!"
interact.s4_multi2 = "Hate is allowed."
interact.s4_multi3 = "Be rude to other players."

--Which answer is needed for the quiz questions. interact.quiz1-4 takes true or false.
--True is left, false is right.
--Please, please spell true and false right!!! If you spell it wrong it won't work!
--interact.quiz can be 1, 2 or 3.
--1 is the top one by the question, 2 is the bottom left one, 3 is the bottom right one.
--Make sure these agree with your answers!
interact.quiz1 = true
interact.quiz2 = false
interact.quiz3 = true
interact.quiz4 = false
interact.quiz_multi = 1
